# Activiti Database 5.22

## 数据库表结构设计

所有的表名默认以“ACT_”开头，列名都是为`_`后缀结尾，防止关键字冲突。

表名的第二部分用两个字母表明表的用例，而这个用例也基本上跟Service API匹配。

> `ACT_GE_* `: 

“GE”代表“General”（通用），表示全局通用数据及设置(general)，各种情况都使用的数据。

> `ACT_HI_*`:

“HI”代表“History”（历史），这些表中保存的都是历史数据，比如执行过的流程实例、变量、任务，等等。Activit默认提供了4种历史级别：

* `none`: 不保存任何历史记录，可以提高系统性能；
* `activity`：保存所有的流程实例、任务、活动信息；
* `audit`：也是Activiti的默认级别，保存所有的流程实例、任务、活动、表单属性；
* `full`：最完整的历史记录，除了包含audit级别的信息之外还能保存详细，例如：流程变量。

对于几种级别根据对功能的要求选择，如果需要日后跟踪详细可以开启full。

> `ACT_ID_*` : 

“ID”代表“Identity”（身份），这些表中保存的都是身份信息，IdentityService接口所操作的表。如用户和组以及两者之间的关系。如果Activiti被集成在某一系统当中的话，这些表可以不用，可以直接使用现有系统中的用户或组信息；

> `ACT_RE_* `: 

“RE”代表“Repository”（仓库），RepositoryService接口所操作的表。这些表中保存一些‘静态’信息，如流程定义和流程资源（如图片、规则等）；

> `ACT_RU_*` : 

“RU”代表“Runtime”（运行时）,运行时表-RuntimeService。，这些表中保存一些流程实例、用户任务、变量等的运行时数据。Activiti只保存流程实例在执行过程中的运行时数据，并且当流程结束后会立即移除这些数据，这是为了保证运行时表尽量的小并运行的足够快；

## 表摘要

表分类 | 表名 | 表描述
---|---|---
一般数据|[`ACT_GE_BYTEARRAY`](#actgebytearray)|通用的流程定义和流程资源
|[`ACT_GE_PROPERTY`](#actgeproperty)|系统相关属性
流程历史记录|[`ACT_HI_ACTINST`](#acthiactinst)|历史的活动信息表
|[`ACT_HI_ATTACHMENT`](#acthiattachment)|历史的流程附件
|[`ACT_HI_COMMENT`](#acthicomment)|历史的说明性信息
|[`ACT_HI_DETAIL`](#acthidetail)|历史的流程运行中的细节信息
|[`ACT_HI_IDENTITYLINK`](#acthiidentitylink)|历史的流程运行过程中用户关系
|[`ACT_HI_PROCINST`](#acthiprocinst)|历史的流程实例
|[`ACT_HI_TASKINST`](#acthitaskinst)|历史的任务实例
|[`ACT_HI_VARINST`](#acthivarinst)|历史的流程运行中的变量信息
用户用户组表|``|
流程定义表|[`ACT_RE_DEPLOYMENT`](#actredeployment)|	部署单元信息
|[`ACT_RE_MODEL`](#actremodel)|模型信息
|[`ACT_RE_PROCDEF`](#actreprocdef)|已部署的流程定义
运行实例表|[`ACT_RU_EXECUTION`](#actrunexecution)|运行时流程执行实例
|[`ACT_RU_IDENTITYLINK`](#actruidentitylink)|已部署的流程定义
|[`ACT_RU_JOB`](#actrujob)|运行时作业
|[`ACT_RU_TASK`](#actrutask)|运行时任务
|[`ACT_RU_VARIABLE`](#actruvariable)|运行时变量表
|[`ACT_RU_EVENT_SUBSCR`](#actrueventsubscr)|运行时事件




## db文档

### `ACT_GE_BYTEARRAY` 

通用的流程定义和流程资源

用来保存部署文件的大文本数据。

保存流程定义图片和xml、Serializable(序列化)的变量,即保存所有二进制数据，特别注意类路径部署时候，不要把svn等隐藏文件或者其他与流程无关的文件也一起部署到该表中，会造成一些错误（可能导致流程定义无法删除）。

```sql

CREATE TABLE `ACT_GE_BYTEARRAY` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL comment '主键ID，资源文件编号，自增长',
  `REV_` int(11) DEFAULT NULL comment '版本号',
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '部署的文件名称, like mail.bpmn、mail.png 、mail.bpmn20.xml',
  `DEPLOYMENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '来自于父表ACT_RE_DEPLOYMENT的主键, 部署的ID',
  `BYTES_` longblob comment '大文本类型，存储文本字节流',
  `GENERATED_` tinyint(4) DEFAULT NULL comment '是否是引擎生成。0为用户生成 1为Activiti生成',
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```

demo数据：

![jpg](/imgs/act_ge_bytearray.jpg)

### `ACT_GE_PROPERTY`

属性数据表，存储整个流程引擎级别的数据,初始化表结构时，会默认插入三条记录。

```sql
CREATE TABLE `ACT_GE_PROPERTY` (
  `NAME_` varchar(64) COLLATE utf8_bin NOT NULL comment '主键schema.version、schema.history、next.dbid',
  `VALUE_` varchar(300) COLLATE utf8_bin DEFAULT NULL comment '例如：5.*、create(5.*)',
  `REV_` int(11) DEFAULT NULL comment '乐观锁, 默认值 NULL，version版本',
  PRIMARY KEY (`NAME_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```

默认的三条数据如下：

![jpg](/imgs/act_ge_property.jpeg)

### `ACT_HI_ACTINST`

历史活动信息或者叫历史节点表。这里记录流程流转过的所有节点（包括开始节点和结束节点)，与HI_TASKINST不同的是，taskinst只记录usertask内容。

```sql
CREATE TABLE `ACT_HI_ACTINST` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin NOT NULL comment '流程定义ID',
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin NOT NULL comment '流程实例ID',
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin NOT NULL comment '执行实例ID',
  `ACT_ID_` varchar(255) COLLATE utf8_bin NOT NULL comment '节点定义ID',
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '节点实例ID, 默认值 NULL，其他节点类型实例ID在这里为空',
  `CALL_PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '调用外部的流程实例ID, 默认值NULL，',
  `ACT_NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '节点名称',
  `ACT_TYPE_` varchar(255) COLLATE utf8_bin NOT NULL comment '节点类型, 如startEvent、userTask',
  `ASSIGNEE_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '签收人, 默认值 NULL，节点签收人',
  `START_TIME_` datetime(3) NOT NULL comment '开始时间, version版本，2016-11-15 11:30:00',
  `END_TIME_` datetime(3) DEFAULT NULL comment '结束时间, 默认值 NULL，2016-11-15 11:30:00',
  `DURATION_` bigint(20) DEFAULT NULL comment '耗时, 默认值 NULL，毫秒值',
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '' comment '租户标识',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_ACT_INST_START` (`START_TIME_`),
  KEY `ACT_IDX_HI_ACT_INST_END` (`END_TIME_`),
  KEY `ACT_IDX_HI_ACT_INST_PROCINST` (`PROC_INST_ID_`,`ACT_ID_`),
  KEY `ACT_IDX_HI_ACT_INST_EXEC` (`EXECUTION_ID_`,`ACT_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```

### `ACT_HI_ATTACHMENT`

历史附件表。

```sql
CREATE TABLE `ACT_HI_ATTACHMENT` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL comment '乐观锁',
  `USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '用户ID',
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '名称',
  `DESCRIPTION_` varchar(4000) COLLATE utf8_bin DEFAULT NULL comment '描述',
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '类型附件类型',
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '节点实例ID',
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '流程实例ID',
  `URL_` varchar(4000) COLLATE utf8_bin DEFAULT NULL comment 'URL附件地址',
  `CONTENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '字节表的ID, 默认值 NULL，ACT_GE_BYTEARRAY的ID',
  `TIME_` datetime(3) DEFAULT NULL comment '乐观锁',
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```

### `ACT_HI_COMMENT`
历史意见表

```sql
CREATE TABLE `ACT_HI_COMMENT` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '类型：event（事件）、comment（意见）',
  `TIME_` datetime(3) NOT NULL comment '填写时间',
  `USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '填写人ID',
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '节点实例ID',
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '流程实例ID',
  `ACTION_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '行为类型, 默认值 NULL，值为下列内容中的一种：AddUserLink、DeleteUserLink、AddGroupLink、DeleteGroupLink、AddComment、AddAttachment、DeleteAttachment',
  `MESSAGE_` varchar(4000) COLLATE utf8_bin DEFAULT NULL comment '基本内容, 用于存放流程产生的信息，比如审批意见',
  `FULL_MSG_` longblob comment '全部内容, 附件地址',
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```

### `ACT_HI_DETAIL`

历史详情表：流程中产生的变量详细，包括控制流程流转的变量，业务表单中填写的流程需要用到的变量等。

```sql
CREATE TABLE `ACT_HI_DETAIL` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL comment '类型, 表单：FormProperty，参数：VariableUpdate',
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '流程实例ID',
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '执行实例ID',
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '节点实例ID',
  `ACT_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '节点实例ID',
  `NAME_` varchar(255) COLLATE utf8_bin NOT NULL comment '名称',
  `VAR_TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '参数类型,  jpa-entity、boolean、bytes、serializable(可序列化)、自定义type(根据你自身配置)、CustomVariableType、date、double、integer、long、null、short、string',
  `REV_` int(11) DEFAULT NULL comment '',
  `TIME_` datetime(3) NOT NULL comment '创建时间',
  `BYTEARRAY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '字节表ID',
  `DOUBLE_` double DEFAULT NULL comment '默认值 NULL，存储变量类型为Double',
  `LONG_` bigint(20) DEFAULT NULL comment '默认值 NULL，存储变量类型为long',
  `TEXT_` varchar(4000) COLLATE utf8_bin DEFAULT NULL comment '默认值 NULL，存储变量值类型为String',
  `TEXT2_` varchar(4000) COLLATE utf8_bin DEFAULT NULL comment '默认值 NULL，此处存储的是JPA持久化对象时，才会有值。此值为对象ID',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_DETAIL_PROC_INST` (`PROC_INST_ID_`),
  KEY `ACT_IDX_HI_DETAIL_ACT_INST` (`ACT_INST_ID_`),
  KEY `ACT_IDX_HI_DETAIL_TIME` (`TIME_`),
  KEY `ACT_IDX_HI_DETAIL_NAME` (`NAME_`),
  KEY `ACT_IDX_HI_DETAIL_TASK_ID` (`TASK_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```

### `ACT_HI_IDENTITYLINK`

```sql
CREATE TABLE `ACT_HI_IDENTITYLINK` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `GROUP_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_IDENT_LNK_USER` (`USER_ID_`),
  KEY `ACT_IDX_HI_IDENT_LNK_TASK` (`TASK_ID_`),
  KEY `ACT_IDX_HI_IDENT_LNK_PROCINST` (`PROC_INST_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```

### `ACT_HI_PROCINST`

历史流程实例表。

```sql
CREATE TABLE `ACT_HI_PROCINST` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '流程实例ID键',
  `BUSINESS_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '业务主键，业务表单的ID',
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '流程定义ID',
  `START_TIME_` datetime(3) NOT NULL COMMENT '开始时间',
  `END_TIME_` datetime(3) DEFAULT NULL COMMENT '结束时间',
  `DURATION_` bigint(20) DEFAULT NULL COMMENT '耗时',
  `START_USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '起草人',
  `START_ACT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '开始节点ID',
  `END_ACT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '结束节点ID',
  `SUPER_PROCESS_INSTANCE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '父流程实例ID',
  `DELETE_REASON_` varchar(4000) COLLATE utf8_bin DEFAULT NULL COMMENT '删除原因',
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '' COMMENT '租户ID',
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `PROC_INST_ID_` (`PROC_INST_ID_`),
  KEY `ACT_IDX_HI_PRO_INST_END` (`END_TIME_`),
  KEY `ACT_IDX_HI_PRO_I_BUSKEY` (`BUSINESS_KEY_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```

### `ACT_HI_TASKINST`

历史流程任务表

```sql
CREATE TABLE `ACT_HI_TASKINST` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '流程定义ID',
  `TASK_DEF_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '节点定义ID',
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '流程实例ID',
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '执行实例ID',
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '名称',
  `PARENT_TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '父节点实例ID',
  `DESCRIPTION_` varchar(4000) COLLATE utf8_bin DEFAULT NULL COMMENT '描述',
  `OWNER_` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '实际签收人 任务的拥有者',
  `ASSIGNEE_` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '签收人或被委托',
  `START_TIME_` datetime(3) NOT NULL COMMENT '开始时间',
  `CLAIM_TIME_` datetime(3) DEFAULT NULL COMMENT '提醒时间',
  `END_TIME_` datetime(3) DEFAULT NULL COMMENT '结束时间',
  `DURATION_` bigint(20) DEFAULT NULL COMMENT '耗时',
  `DELETE_REASON_` varchar(4000) COLLATE utf8_bin DEFAULT NULL COMMENT '删除原因',
  `PRIORITY_` int(11) DEFAULT NULL COMMENT '优先级别',
  `DUE_DATE_` datetime(3) DEFAULT NULL COMMENT '过期时间，表明任务应在多长时间内完成',
  `FORM_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '节点定义的formkey,desinger节点定义的form_key属性',
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '类别',
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '' COMMENT '租户ID',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_TASK_INST_PROCINST` (`PROC_INST_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```

### `ACT_HI_VARINST`

流程历史变量表

```sql
CREATE TABLE `ACT_HI_VARINST` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '流程实例ID',
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '执行实例ID',
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '节点实例ID',
  `NAME_` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '名称',
  `VAR_TYPE_` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '参数类型, jpa-entity、boolean、bytes、serializable、自定义type(根据你自身配置)、CustomVariableType、date、double、integer、jpa-entity、long、null、short、string',
  `REV_` int(11) DEFAULT NULL COMMENT '乐观锁',
  `BYTEARRAY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '字节表ID, ACT_GE_BYTEARRAY表的主键',
  `DOUBLE_` double DEFAULT NULL COMMENT '存储DoubleType类型的数据',
  `LONG_` bigint(20) DEFAULT NULL COMMENT '存储LongType类型的数据',
  `TEXT_` varchar(4000) COLLATE utf8_bin DEFAULT NULL COMMENT '存储变量值类型为String，如此处存储持久化对象时，值jpa对象的class',
  `TEXT2_` varchar(4000) COLLATE utf8_bin DEFAULT NULL COMMENT '此处存储的是JPA持久化对象时，才会有值。此值为对象ID',
  `CREATE_TIME_` datetime(3) DEFAULT NULL COMMENT '创建时间',
  `LAST_UPDATED_TIME_` datetime(3) DEFAULT NULL COMMENT '最新更改时间',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_PROCVAR_PROC_INST` (`PROC_INST_ID_`),
  KEY `ACT_IDX_HI_PROCVAR_NAME_TYPE` (`NAME_`,`VAR_TYPE_`),
  KEY `ACT_IDX_HI_PROCVAR_TASK_ID` (`TASK_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```

### `ACT_RE_DEPLOYMENT`

部署信息表，用来存储部署时需要持久化保存下来的信息

```sql
`CREATE TABLE `ACT_RE_DEPLOYMENT` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL comment '部署编号，自增长',
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '部署包的名称',
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '类型',
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '' comment '租户, 多租户通常是在软件需要为多个不同组织服务时产生的概念',
  `DEPLOY_TIME_` timestamp(3) NULL DEFAULT NULL comment '部署时间',
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```

![jpg](/imgs/act-re-deployment.jpg)

### `ACT_RE_MODEL`

流程设计模型部署表, 流程设计器设计流程后，保存数据到该表。

```sql
CREATE TABLE `ACT_RE_MODEL` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL comment '乐观锁, 默认值 NULL，version版本',
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '部署文件名称',
  `KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '例如：http://www.mossle.com/docs/activiti/',
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '分类',
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL comment '创建时间',
  `LAST_UPDATE_TIME_` timestamp(3) NULL DEFAULT NULL comment '最新修改时间',
  `VERSION_` int(11) DEFAULT NULL comment '版本',
  `META_INFO_` varchar(4000) COLLATE utf8_bin DEFAULT NULL comment '以json格式保存流程定义的信息',
  `DEPLOYMENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '部署ID',
  `EDITOR_SOURCE_VALUE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment 'ACT_GE_BYTEARRAY的ID',
  `EDITOR_SOURCE_EXTRA_VALUE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment 'ACT_GE_BYTEARRAY的ID',
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '' comment '租户ID',
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```

### `ACT_RE_PROCDEF`

已部署的流程定义（流程定义：解析表）

流程解析表，解析成功了，在该表保存一条记录。业务流程定义数据表

```sql
CREATE TABLE `ACT_RE_PROCDEF` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL COMMENT '版本号',
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '流程命名空间（该编号就是流程文件targetNamespace的属性值）',
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '流程名称（该编号就是流程文件process元素的name属性值）',
  `KEY_` varchar(255) COLLATE utf8_bin NOT NULL comment '流程编号（该编号就是流程文件process元素的id属性值）',
  `VERSION_` int(11) NOT NULL comment '流程版本号（由程序控制，新增即为1，修改后依次加1来完成的）',
  `DEPLOYMENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '部署编号 部署表ID',
  `RESOURCE_NAME_` varchar(4000) COLLATE utf8_bin DEFAULT NULL comment '资源文件名称 流程bpmn文件名称',
  `DGRM_RESOURCE_NAME_` varchar(4000) COLLATE utf8_bin DEFAULT NULL comment '图片资源文件名称, png流程图片名称',
  `DESCRIPTION_` varchar(4000) COLLATE utf8_bin DEFAULT NULL comment '描述信息, 描述',
  `HAS_START_FORM_KEY_` tinyint(4) DEFAULT NULL comment '是否从key启动, start节点是否存在formKey0否  1是',
  `HAS_GRAPHICAL_NOTATION_` tinyint(4) DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) DEFAULT NULL comment '是否挂起, 1激活 2挂起',
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '' comment '租户ID',
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```
> 注：
> * 此表和`ACT_RE_DEPLOYMENT`是多对一的关系，即，一个部署的bar包里可能包含多个流程定义文件，每个流程定义文件都会有一条记录在`ACT_RE_PROCDEF`表内，
> * 每个流程定义的数据，都会对于`ACT_GE_BYTEARRAY`表内的一个资源文件和PNG图片文件。和`ACT_GE_BYTEARRAY`的关联是通过程序用`ACT_GE_BYTEARRAY.NAME`与`ACT_RE_PROCDEF.NAME_`完成的，在数据库表结构中没有体现。

ID_|REV_|CATEGORY_|NAME_|KEY_|VERSION_|DEPLOYMENT_ID_|RESOURCE_NAME_|DGRM_RESOURCE_NAME_|DESCRIPTION_|HAS_START_FORM_KEY_|HAS_GRAPHICAL_NOTATION_|SUSPENSION_STATE_|TENANT_ID_
---|---|---|---|---|---|---|---|---|---|---|---|---|---
myProcess_1:1:2504|	1|	http://www.activiti.org/test|null|		myProcess_1|	1|	2501|	processes/bpmn-demo.bpmn20.xml	|processes/bpmn-demo.myProcess_1.png|null|		0|	1|	1	|
process:1:4|	1|	http://www.activiti.org/processdef	|null|	process|	1|	1|	/Users/wychuan/codes/bbt/Polaris/Staff/staff-web/target/classes/processes/process.bpmn20.xml|	/Users/wychuan/codes/bbt/Polaris/Staff/staff-web/target/classes/processes/process.process.png|null|		0|	1|	1|	



### `ACT_RU_EXECUTION`

流程执行记录表。

```sql
CREATE TABLE `ACT_RU_EXECUTION` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL comment '主键ID',
  `REV_` int(11) DEFAULT NULL comment '乐观锁, 默认值 NULL，version版本',
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '流程实例ID',
  `BUSINESS_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '业务主键ID',
  `PARENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '父节点实例ID',
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '流程定义ID',
  `SUPER_EXEC_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '节点实例ID, 节点实例ID即ACT_HI_ACTINST中ID',
  `IS_ACTIVE_` tinyint(4) DEFAULT NULL comment '是否存活	',
  `IS_CONCURRENT_` tinyint(4) DEFAULT NULL comment '是否并行(true/false）',
  `IS_SCOPE_` tinyint(4) DEFAULT NULL,
  `IS_EVENT_SCOPE_` tinyint(4) DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) DEFAULT NULL comment '挂起状态（1：激活、2：挂起）',
  `CACHED_ENT_STATE_` int(11) DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '' comment '租户ID',
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '名称',
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_EXEC_BUSKEY` (`BUSINESS_KEY_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```

### `ACT_RU_IDENTITYLINK`

运行时流程人员表，主要存储任务节点与参与者相关信息

```sql
CREATE TABLE `ACT_RU_IDENTITYLINK` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL comment '乐观锁',
  `GROUP_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '用户组ID',
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '类型:assignee、candidate、owner、starter、participant',
  `USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '用户iD',
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '节点实例ID',
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '流程实例ID',
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '流程定义ID',
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```

### `ACT_RU_JOB`

运行时定时任务数据表

```sql
CREATE TABLE `ACT_RU_JOB` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL comment '主键',
  `REV_` int(11) DEFAULT NULL comment '乐观锁',
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL comment '类型',
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL comment '锁定释放时间',
  `LOCK_OWNER_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '挂起者',
  `EXCLUSIVE_` tinyint(1) DEFAULT NULL comment '', 
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '执行实例ID',
  `PROCESS_INSTANCE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '流程实例ID',
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '流程定义ID',
  `RETRIES_` int(11) DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '异常信息ID',
  `EXCEPTION_MSG_` varchar(4000) COLLATE utf8_bin DEFAULT NULL comment '异常信息',
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL comment '到期时间',
  `REPEAT_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '重复',
  `HANDLER_TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '处理类型',
  `HANDLER_CFG_` varchar(4000) COLLATE utf8_bin DEFAULT NULL comment '处理标识',
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '' comment '租户ID',
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```

### `ACT_RU_TASK`

运行时任务节点表

```sql
CREATE TABLE `ACT_RU_TASK` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL comment '主键',
  `REV_` int(11) DEFAULT NULL comment '乐观锁',
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '执行实例ID',
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '流程实例ID',
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '流程定义ID',
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '节点定义名称',
  `PARENT_TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '父节点实例ID',
  `DESCRIPTION_` varchar(4000) COLLATE utf8_bin DEFAULT NULL comment '节点定义描述',
  `TASK_DEF_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '节点定义的KEY, 任务定义的ID',
  `OWNER_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '实际签收人, 拥有者（一般情况下为空，只有在委托时才有值）',
  `ASSIGNEE_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '签收人或委托人',
  `DELEGATION_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '委托类型, DelegationState分为两种：PENDING，RESOLVED，如无委托则为空。',
  `PRIORITY_` int(11) DEFAULT NULL comment '优先级别，默认为：50',
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL comment '创建时间',
  `DUE_DATE_` datetime(3) DEFAULT NULL comment '过期时间',
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '类别',
  `SUSPENSION_STATE_` int(11) DEFAULT NULL comment '（1：代表激活、2：代表挂起）',
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '' comment '租户ID',
  `FORM_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL comment '节点定义的formkey, 表单KEY',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_TASK_CREATE` (`CREATE_TIME_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```

### `ACT_RU_VARIABLE`

运行时流程变量数据表。

```sql

CREATE TABLE `ACT_RU_VARIABLE` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL comment '主键',
  `REV_` int(11) DEFAULT NULL comment '乐观锁',
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL comment '类型, jpa-entity、boolean、bytes、serializable、自定义type(根据你自身配置)、CustomVariableType、date、double、integer、jpa-entity、long、null、short、string',
  `NAME_` varchar(255) COLLATE utf8_bin NOT NULL comment '变量名称',
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '执行实例ID',
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '流程实例ID',
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment '节点实例ID',
  `BYTEARRAY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL comment 'ACT_GE_BYTEARRAY的ID_',
  `DOUBLE_` double DEFAULT NULL comment '存储变量类型为Double',
  `LONG_` bigint(20) DEFAULT NULL,
  `TEXT_` varchar(4000) COLLATE utf8_bin DEFAULT NULL comment '‘存储变量值类型为String,如此处存储持久化对象时，值jpa对象的class',
  `TEXT2_` varchar(4000) COLLATE utf8_bin DEFAULT NULL comment '此处存储的是JPA持久化对象时，才会有值。此值为对象ID',
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```

# 参考

* http://lucaslz.com/2016/11/15/java/activiti/activiti-db-5-22/
* http://blog.csdn.net/hj7jay/article/details/51302829