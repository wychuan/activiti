# Sequence Flow顺序流

## 顺序流

### Description

顺序流是连接两个结点的连线，流程执行完一个节点后，会沿着节点的所有外出顺序流继续执行。

BPMN 2.0 默认的行为就是支持并发的： 两个外出的顺序流会创造两个单独的，并发流程分支。

### Graphical notation

![png](/imgs/bpmn.sequence.flow.png)

从起点元素指向终点元素的箭头图标。

### XML representation

```xml
<sequenceFlow id="flow1" sourceRef="theStart" targetRef="theTask" />
```

sequenceFlow xml标签，必须指定一个process范围内的唯一ID及起点和终点元素的引用。
* `id`: process范围内唯一ID
* `sourceRef`: 起点元素引用
* `targetRef`: 终点元素引用

## 条件顺序流（Conditional sequence flow）

### Description

在离开一个BPMN2.0节点时，默认会计算外出顺序流的条件，如果条件为true，就选择此外出顺序流继续执行。

如果多条外出顺序流条件都为true，就会创建多条流程分支，以并发形式继续执行。

> 注意：上面的面容只针对BPMN2.0节点，不包含网关。网关有自己特有的处理流程的方式。

### Graphical notation

![png](/imgs/bpmn.conditional.sequence.flow.png)

在普通顺序流图标的基础上，在起点位置添加了一个菱形。

### XML representation

```xml
<sequenceFlow id="flow" sourceRef="theStart" targetRef="theTask">
  <conditionExpression xsi:type="tFormalExpression">
    <![CDATA[${order.price > 100 && order.price < 250}]]>
  </conditionExpression>
</sequenceFlow>
```

在正常的顺序流的基础上，添加`conditionExpression`元素。

> 注意： 目前只支持 `tFormalExpressions`， 如果没有设置xsi:type="" , 就会默认值支持目前支持的表达式类型

当前条件表达式仅支持 UEL。条件表达式应该返回boolean值，否则会在解析表达式时抛出异常；

如：

```xml
<conditionExpression xsi:type="tFormalExpression">
  <![CDATA[${order.price > 100 && order.price < 250}]]>
</conditionExpression>
```

```xml
<conditionExpression xsi:type="tFormalExpression">
  <![CDATA[${order.isStandardOrder()}]]>
</conditionExpression>
```


## 默认顺序流

### Description

默认顺序流可以理解为`switch`语句中的`default`块，在其他所有的流出顺序流都不被选中时执行。默认顺序流的条件设置不会生效。

### Graphical notation

![png](/imgs/bpmn.default.sequence.flow.png)

### XML representation

```xml
<exclusiveGateway id="exclusiveGw" name="Exclusive Gateway" default="flow2" />
<sequenceFlow id="flow1" sourceRef="exclusiveGw" targetRef="task1">
  <conditionExpression xsi:type="tFormalExpression">${conditionA}</conditionExpression>
</sequenceFlow>
<sequenceFlow id="flow2" sourceRef="exclusiveGw" targetRef="task2"/>
<sequenceFlow id="flow3" sourceRef="exclusiveGw" targetRef="task3">
  <conditionExpression xsi:type="tFormalExpression">${conditionB}</conditionExpression>
</sequenceFlow>
```

默认顺序流通过对应节点的`default`属性进行设置。上面排他网关设置了默认顺序流为`flow2`，在`flow1	`的`conditionA`和`flow3`的`conditionB`都不满足条件时，会执行`flow2`。