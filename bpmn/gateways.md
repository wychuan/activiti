# Gateways网关

## Exclusive Gateway 排他网关

#### Description 描述

排他网关（也叫异或（XOR）网关，或更技术性的叫法 基于数据的排他网关）， 用来在流程中实现决策。

当流程执行到这个网关，所有外出顺序流都会被处理一遍。 其中条件解析为true的顺序流（或者没有设置条件，概念上在顺序流上定义了一个'true'） 会被选中，让流程继续运行。

> 注意：BPMN 2.0 通常情况下，所有条件结果为true的顺序流 都会被选中，以并行方式执行，但排他网关只会选择一条顺序流执行。 就是说，虽然多个顺序流的条件结果为true， 那么XML中的第一个顺序流（也只有这一条）会被选中，并用来继续运行流程。 如果没有选中任何顺序流，会抛出一个异常

#### Graphical notation 图形标记
排他网关显示成一个普通网关（比如，菱形图形）， 内部是一个“X”图标，表示异或（XOR）语义。

![png](/imgs/bpmn.exclusive.gateway.notation.png)

> 注意，没有内部图标的网关，默认为排他网关。 BPMN 2.0 规范不允许在同一个流程定义中同时使用没有X和有X的菱形图形。

#### XML representation

```xml
<exclusiveGateway id="exclusiveGw" name="Exclusive Gateway" />

<sequenceFlow id="flow2" sourceRef="exclusiveGw" targetRef="theTask1">
  <conditionExpression xsi:type="tFormalExpression">${input == 1}</conditionExpression>
</sequenceFlow>

<sequenceFlow id="flow3" sourceRef="exclusiveGw" targetRef="theTask2">
  <conditionExpression xsi:type="tFormalExpression">${input == 2}</conditionExpression>
</sequenceFlow>

<sequenceFlow id="flow4" sourceRef="exclusiveGw" targetRef="theTask3">
  <conditionExpression xsi:type="tFormalExpression">${input == 3}</conditionExpression>
</sequenceFlow>
```

## Parallel Gateway 并行网关

#### Description

并行网关可以表示流程中的并发情况。它允许将流程分成多条分支，也可以把多条分支汇聚到一起。

并行网关的功能是基于流入和流出的顺序流的：

* 分支：所有流出的顺序流并行执行，为每个顺序流创建一个并发分支。
* 汇聚：到达并行网关的所有执行都在并行网关中等待，直到所有的流入顺序流执行完毕。然后流程会通过并行网关。

> 注意：与其他网关的主要区别是，并行网关不会解析条件。 即使顺序流中定义了条件，也会被忽略。

### Graphical notation

![png](/imgs/bpmn.parallel.gateway.png)

### XML representation

定义一个并行网关如下：
```xml
<parallelGateway id="myParallelGateway" />
```
实际发生的行为（分支，聚合，同时分支聚合）， 要根据并行网关的顺序流来决定。

```xml
<startEvent id="theStart" />
<sequenceFlow id="flow1" sourceRef="theStart" targetRef="fork" />

<parallelGateway id="fork" />
<sequenceFlow sourceRef="fork" targetRef="receivePayment" />
<sequenceFlow sourceRef="fork" targetRef="shipOrder" />

<userTask id="receivePayment" name="Receive Payment" />
<sequenceFlow sourceRef="receivePayment" targetRef="join" />

<userTask id="shipOrder" name="Ship Order" />
<sequenceFlow sourceRef="shipOrder" targetRef="join" />

<parallelGateway id="join" />
<sequenceFlow sourceRef="join" targetRef="archiveOrder" />

<userTask id="archiveOrder" name="Archive Order" />
<sequenceFlow sourceRef="archiveOrder" targetRef="theEnd" />

<endEvent id="theEnd" />
```

上面例子中，流程启动之后，会创建两个任务：

```java
ProcessInstance pi = runtimeService.startProcessInstanceByKey("forkJoin");
TaskQuery query = taskService.createTaskQuery()
                         .processInstanceId(pi.getId())
                         .orderByTaskName()
                         .asc();

List<Task> tasks = query.list();
assertEquals(2, tasks.size());

Task task1 = tasks.get(0);
assertEquals("Receive Payment", task1.getName());
Task task2 = tasks.get(1);
assertEquals("Ship Order", task2.getName());
```

## Inclusive Gateway 包含网关

#### Description

包含网关可以看成是排他网关和并行网关的结合体，和排他网关一样，可以在外出顺序流上定义条件，包含网关会解析它们。但是和包含网关的主要区别是可以选择多条顺序流并发执行，这是并行网关一样。

并行网关的功能是基于流入和流出的顺序流的：

* 分支：所有外出顺序流的条件都会被解析，结果为true的顺序流会以并行方式继续执行，会为每个顺序流创建一个并发分支。
* 汇聚：到达包含网关的所有执行都在此等待，直到每个包含token的流入顺序流执行完毕都到达，然后流程会通过包含网关。此处是与并行网关的最大不同，并行网关是等待所有流入顺序流到达，而包含网关是所有被选择的流入顺序流到达。


### Graphical notation

![png](/imgs/bpmn.inclusive.gateway.png)

### XML representation

定义一个包含网关只需要一句xml:
```xml
<inclusiveGateway id="myInclusiveGateway" />
```

实际的行为（分支，汇聚或同时分支汇聚）， 是由连接在包含网关的顺序流决定的

```xml
<startEvent id="theStart" />
<sequenceFlow id="flow1" sourceRef="theStart" targetRef="fork" />

<inclusiveGateway id="fork" />
<sequenceFlow sourceRef="fork" targetRef="receivePayment" >
  <conditionExpression xsi:type="tFormalExpression">${paymentReceived == false}</conditionExpression>
</sequenceFlow>
<sequenceFlow sourceRef="fork" targetRef="shipOrder" >
  <conditionExpression xsi:type="tFormalExpression">${shipOrder == true}</conditionExpression>
</sequenceFlow>

<userTask id="receivePayment" name="Receive Payment" />
<sequenceFlow sourceRef="receivePayment" targetRef="join" />

<userTask id="shipOrder" name="Ship Order" />
<sequenceFlow sourceRef="shipOrder" targetRef="join" />

<inclusiveGateway id="join" />
<sequenceFlow sourceRef="join" targetRef="archiveOrder" />

<userTask id="archiveOrder" name="Archive Order" />
<sequenceFlow sourceRef="archiveOrder" targetRef="theEnd" />

<endEvent id="theEnd" />
```

在上面的例子中，流程开始之后，如果流程变量为 paymentReceived == false 和 shipOrder == true， 就会创建两个任务。如果，只有一个流程变量为true，就会只创建一个任务。 如果没有条件为true，就会抛出一个异常。 如果想避免异常，可以定义一个默认顺序流。下面的例子中，会创建一个任务，发货任务：

```java
HashMap<String, Object> variableMap = new HashMap<String, Object>();
          variableMap.put("receivedPayment", true);
          variableMap.put("shipOrder", true);
          ProcessInstance pi = runtimeService.startProcessInstanceByKey("forkJoin");
TaskQuery query = taskService.createTaskQuery()
                         .processInstanceId(pi.getId())
                         .orderByTaskName()
                         .asc();

List<Task> tasks = query.list();
assertEquals(1, tasks.size());

Task task = tasks.get(0);
assertEquals("Ship Order", task.getName());
```

## Event-based Gateway 基于事件网关

#### Description

基于事件网关允许根据事件判断流向。网关的每个外出顺序流都要连接到一个中间捕获事件。 当流程到达一个基于事件网关，网关会进入等待状态：会暂停执行。 与此同时，会为每个外出顺序流创建相对的事件订阅。

注意基于事件网关的外出顺序流和普通顺序流不同。这些顺序流不会真的"执行"。 相反，它们让流程引擎去决定执行到基于事件网关的流程需要订阅哪些事件。 要考虑以下条件：

* 基于事件网关必须有两条或以上外出顺序流。
* 基于事件网关后，只能使用 intermediateCatchEvent 类型。 （activiti 不支持基于事件网关后连接 ReceiveTask。）
* 连接到基于事件网关的 intermediateCatchEvent 只能有一条进入顺序流。

### Graphical notation

![png](/imgs/bpmn.event.based.gateway.notation.png)

### XML representation

用来定义基于事件网关的XML元素是 eventBasedGateway。

### Example(s) 实例


# 参考

* [Sequence Flow](https://www.activiti.org/userguide/index.html#bpmnConditionalSequenceFlow)
* [顺序流](https://doc.yonyoucloud.com/doc/activiti-5.x-user-guide/Chapter%208.%20BPMN%202.0%20Constructs%20%E5%85%B3%E4%BA%8E%20BPMN%202.0%20%E6%9E%B6%E6%9E%84/Sequence%20Flow%20%E9%A1%BA%E5%BA%8F%E6%B5%81.html)
* https://www.activiti.org/userguide/index.html#bpmnGateways
* https://doc.yonyoucloud.com/doc/activiti-5.x-user-guide/Chapter%208.%20BPMN%202.0%20Constructs%20%E5%85%B3%E4%BA%8E%20BPMN%202.0%20%E6%9E%B6%E6%9E%84/Gateways%20%E7%BD%91%E5%85%B3.html

