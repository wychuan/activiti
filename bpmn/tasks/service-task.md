# Java Service Task服务任务

## Description 描述

Java 服务任务用来调用外部 Java 类

## Graphical notation 图形标记

![png](/imgs/bpmn.java.service.task.png)

## XML representation

有4钟方法来声明 java 调用逻辑：

* 实现 JavaDelegate 或 ActivityBehavior
```xml
<serviceTask id="javaService" 
             name="My Java Service Task" 
             activiti:class="org.activiti.MyJavaDelegate" />
```
* 执行解析代理对象的表达式
	也可以使用表达式调用一个对象。对象必须遵循一些规则， 并使用activiti:class 属性进行创建。
```xml
<serviceTask id="serviceTask" activiti:delegateExpression="${delegateExpressionBean}" />
```
* 调用一个方法表达式
```xml
<serviceTask id="javaService" 
         name="My Java Service Task" 
         activiti:expression="#{printer.printMessage()}" />   
```
* 调用一个值表达式
```xml
<serviceTask id="javaService" 
         name="My Java Service Task" 
         activiti:expression="#{printer.printMessage(execution, myVar)}" />
```

