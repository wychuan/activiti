# User Task用户任务

## Description 描述

> A user task is used to model work that needs to be done by a human actor. When the process execution arrives at such a user task, a new task is created in the task list of the user(s) or group(s) assigned to that task.

用户任务用来设置必须由人员完成的工作。当流程执行到用户任务，会创建一个新任务，并把这个新任务加入到分配人或群组的任务列表中。

## Graphical notation 图形标记

> A user task is visualized as a typical task (rounded rectangle), with a small user icon in the left upper corner.

用户任务显示成一个普通任务（圆角矩形），左上角有一个小用户图标

![bpmn.user.task.png](/imgs/bpmn.user.task.png)

## XML representation xml表示

> A user task is defined in XML as follows. The id attribute is required, the name attribute is optional.

XML 中的用户任务定义如下。id 属性是必须的。name 属性是可选的。

```xml
<userTask id="theTask" name="Important task" /> 
```

> A user task can have also a description. In fact any BPMN 2.0 element can have a description. A description is defined by adding the documentation element.

用户任务也可以设置描述。实际上所有 BPMN 2.0 元素 都可以设置描述。 添加 documentation 元素可以定义描述。

```xml
<userTask id="theTask" name="Schedule meeting" >
<documentation>
  Schedule an engineering meeting for next week with the new hire.
</documentation>
```
> The description text can be retrieved from the task in the standard Java way:

描述文本可以通过标准的java方法来获得:

```xml
task.getDescription()
```

# 参考资料

* https://www.activiti.org/userguide/index.html#bpmnUserTask