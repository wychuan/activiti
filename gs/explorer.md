# Activiti Explorer 快速搭建一个Demo应用

## 下载

从[Activiti官网](https://www.activiti.org/download-links)下载Activiti，这里下载的版本是`activiti-5.22.0`。

下载之后，解压到本地，可以发现解压后的目录结构如下：

![activiti-5.22](/imgs/activiti-download-files.jpeg)

## 准备环境

* Java运行时
* Tomcat(或者其他web容器)

## 安装

1. 把下载文件war目录下的`activiti-explorer.war`文件复制到`tomcat`的`webapps`目录下；

	```bash
	$ sudo cp ~/soft/activiti/activiti-5.22.0/wars/activiti-explorer.war /usr/local/tomcat/webapps/
	```

2. 启动tomcat

	```bash
	$ sudo bin/startup.sh
	Using CATALINA_BASE:   /usr/local/source/apache-tomcat-8.0.33
	Using CATALINA_HOME:   /usr/local/source/apache-tomcat-8.0.33
	Using CATALINA_TMPDIR: /usr/local/source/apache-tomcat-8.0.33/temp
	Using JRE_HOME:        /Library/Java/JavaVirtualMachines/jdk1.8.0_111.jdk/Contents/Home
	Using CLASSPATH:       /usr/local/source/apache-tomcat-8.0.33/bin/bootstrap.jar:/usr/local/source/apache-tomcat-8.0.33/bin/tomcat-juli.jar
	Tomcat started.
	```

3. Tomcat 启动后，打开浏览器访问 http://localhost:8080/activiti-explorer

## 数据库更改

Activiti Explorer 默认使用 H2 内存数据库，已经包含了示例流程，用户和群组信息。

如果想切换到其它数据库，可以修改 Activiti Explorer web 应用 WEB-INF/ classes 目录下的 db.properties。

```bash
$ cd /usr/local/tomcat/webapps/activiti-explorer/WEB-INF/classes
$ ls
activiti-custom-context.xml org
activiti-login-context.xml  rebel.xml
activiti-ui-context.xml     stencilset.json
db.properties               ui.properties
engine.properties           ui.properties.alfresco
log4j.properties
$ vim db.properties
db=h2
jdbc.driver=org.h2.Driver
jdbc.url=jdbc:h2:mem:activiti;DB_CLOSE_DELAY=1000
jdbc.username=sa
jdbc.password=
```

把上述db.properties数据库相关配置信息更改为你想要连接的数据库配置， 如本地的mysql:

```
db=mysql
jdbc.driver=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3306/activiti?useSSL=false
jdbc.username=sa
jdbc.password=123456
```

> 备注：1. 由于activiti explorer.war默认使用内存数据库 h2，所以在其WEB-INF/libs目录下缺少mysql驱动包，需要复制一个mysql驱动jar到此目录。
> 2. 请提前创建数据库activiti，不需要创建表；

## 登录账号

用户名 | 密码 | 角色
---|---|---
kermit	|kermit|	admin
gonzo	|gonzo	|manager
fozzie	|fozzie	|user

> 注意： Activiti Explorer 演示实例只是一种简单快速展示 Activiti 的功能的方式。 但是并不是说只能使用这种方式使用 Activiti。 Activiti 只是一个 jar， 可以内嵌到任何 Java 环境 中：swing 或者 Tomcat, JBoss, WebSphere 等等。 也可以把Activiti 作为一个典型的单独运行的 BPM 服务器运行。 只要 java 可以做的，Activiti也可以。