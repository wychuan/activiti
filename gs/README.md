# Activiti Get Start

Activiti快速入门可以从下来两件事开始：

1. 下载activiti，使用其提供的explorer.war快速在本地搭建一个demo程序；详情参考 [1.1 Activiti Explorer.war](explorer.md)

2. 使用Activiti API实现一个简单的审批流，了解activiti如何嵌入到本地应用中， 详情参考 [1.2 Activiti API快速入门](api-qs.md)

