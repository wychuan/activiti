# 快速入门

## 简介

![process](/imgs/qs-process.png)

如上图，本实例主要实现一个网站登录的过程，登录对年龄有要求，如果满18岁，允许其登录，否则劝其离开：

1. 输入姓名和名称
2. 如果年龄大于等于18岁，则欢迎用户登录此站（什么站就不说了），祝其玩得愉快；
3. 如果年龄小于18岁，告诉其年龄太小，劝其离开。

## 环境准备

* Maven
* Java运行环境

## 快速入门

### 创建maven项目

最终项目结构如下：
```bash
quick-start
--src
----main
------java
--------samples.activiti
----------WebsiteLogonProcessEngine.java
----------YoungManProcess.java
------resources
--------logon-bpmn20.xml
--------logback.xml
--pom.xml
```

### pom.xml

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>samples.activiti</groupId>
    <artifactId>quick-start</artifactId>
    <version>1.0-SNAPSHOT</version>

    <packaging>jar</packaging>

    <name>quick-start</name>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <activiti.version>5.22.0</activiti.version>
        <h2db.version>1.4.193</h2db.version>
        <slf4j.version>1.7.21</slf4j.version>
        <logback.version>1.1.7</logback.version>
    </properties>


   <dependencies>
        <!-- activiti engine -->
        <dependency>
            <groupId>org.activiti</groupId>
            <artifactId>activiti-engine</artifactId>
            <version>${activiti.version}</version>
        </dependency>

        <!-- h2database -->
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <version>${h2db.version}</version>
        </dependency>

        <!-- <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.41</version>
        </dependency> -->

        <!-- slf4j -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <!-- <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>${slf4j.version}</version>
        </dependency> -->

        <!-- logback -->
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>${logback.version}</version>
        </dependency>
    </dependencies>
</project>
```

### 创建流程引擎

创建`WebsiteLogonProcessEngine.java`文件，数据库使用h2database:

```java
package samples.activiti;

import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.FormData;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.impl.cfg.StandaloneProcessEngineConfiguration;
import org.activiti.engine.impl.form.DateFormType;
import org.activiti.engine.impl.form.LongFormType;
import org.activiti.engine.impl.form.StringFormType;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * 流程引擎入门DEMO, 此DEMO实现流程如下:
 * 1. 输入姓名和名称
 * 2. 如果年龄大于等于18岁，则欢迎用户登录此站（什么站就不说了），祝其玩得愉快；
 * 3. 如果年龄小于18岁，告诉其年龄太小，劝其离开。
 *
 * @author zilaiye
 * @date 2017/10/27
 */
public class WebsiteLogonProcessEngine {
    public static final Logger logger = LoggerFactory.getLogger(WebsiteLogonProcessEngine.class);

    public static void main(String[] args) throws ParseException {
        // 1. 配置 Activiti 流程引擎
        ProcessEngineConfiguration cfg = new StandaloneProcessEngineConfiguration()
            .setJdbcUrl("jdbc:h2:mem:activiti;DB_CLOSE_DELAY=1000")
            .setJdbcUsername("sa")
            .setJdbcPassword("")
            .setJdbcDriver("org.h2.Driver")
            .setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);
        // 2. 获取 Activiti 流程引擎对象
        ProcessEngine processEngine = cfg.buildProcessEngine();
        String pName = processEngine.getName();
        String ver = ProcessEngine.VERSION;
        System.out.println("获取流程引擎对象完成: ProcessEngine Name: [ " + pName + " ] Version: [ " + ver + " ]");

        // 3. 加载流程定义
        RepositoryService repositoryService = processEngine.getRepositoryService();
        Deployment deployment = repositoryService.createDeployment().addClasspathResource("logon-bpmn20.xml").deploy();
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().deploymentId(deployment.getId()).singleResult();
        System.out.println("发现流程定义, 名称:[" + processDefinition.getName() + "] Id:[" + processDefinition.getId() + "]");
////
        // 4. 运营流程实例
        RuntimeService runtimeService = processEngine.getRuntimeService();
        // 启动一个流程实例
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("yellow-site-logon");
        System.out.println("登录判断流程开始,流程实例ID:[" + processInstance.getProcessDefinitionId() + "] key:[" + processInstance.getProcessDefinitionKey() + "]");

        // 流程中的任务
        TaskService taskService = processEngine.getTaskService();
        FormService formService = processEngine.getFormService();
        HistoryService historyService = processEngine.getHistoryService();

        Scanner scanner = new Scanner(System.in);
        while (processInstance != null && !processInstance.isEnded()) {
            // 获取流程的所有运行任务
            List<Task> tasks = taskService.createTaskQuery().taskCandidateGroup("managers").list();
            System.out.println("此流程共有任务数量: [ " + tasks.size() + "]");
            // 循环任务执行之
            for (int i = 0; i < tasks.size(); i++) {
                Task task = tasks.get(i);
                System.out.println("开始执行流程任务: [" + task.getName() + "]");
                Map<String, Object> variables = new HashMap<String, Object>();
                FormData formData = formService.getTaskFormData(task.getId());
                for (FormProperty formProperty : formData.getFormProperties()) {
                    if (StringFormType.class.isInstance(formProperty.getType())) {
                        System.out.println(formProperty.getName() + "?");
                        String value = scanner.nextLine();
                        variables.put(formProperty.getId(), value);
                    } else if (LongFormType.class.isInstance(formProperty.getType())) {
                        System.out.println(formProperty.getName() + "? (Must be a whole number)");
                        Long value = Long.valueOf(scanner.nextLine());
                        variables.put(formProperty.getId(), value);
                    } else if (DateFormType.class.isInstance(formProperty.getType())) {
                        System.out.println(formProperty.getName() + "? (Must be a date m/d/yy)");
                        DateFormat dateFormat = new SimpleDateFormat("m/d/yy");
                        Date value = dateFormat.parse(scanner.nextLine());
                        variables.put(formProperty.getId(), value);
                    } else {
                        System.out.println("<form type not supported>");
                    }
                }
                taskService.complete(task.getId(), variables);

                System.out.println("流程处理完成, 现在开始回放处理过程:");
                HistoricActivityInstance endActivity = null;
                List<HistoricActivityInstance> activities = historyService.createHistoricActivityInstanceQuery()
                    .processInstanceId(processInstance.getId()).finished()
                    .orderByHistoricActivityInstanceEndTime().asc()
                    .list();
                for (HistoricActivityInstance activity : activities) {
                    if (activity.getActivityType() == "startEvent") {
                        System.out.println("流程处理开始, 实例名称:[" + processDefinition.getName() + "] Key:[" + processInstance.getProcessDefinitionKey() + "] 开始处理时间: [" + activity.getStartTime() + "]");
                    }
                    if (activity.getActivityType() == "endEvent") {
                        // Handle edge case where end step happens so fast that the end step
                        // and previous step(s) are sorted the same. So, cache the end step
                        //and display it last to represent the logical sequence.
                        endActivity = activity;
                    } else {
                        System.out.println("-- " + activity.getActivityName()
                            + " [" + activity.getActivityId() + "] "
                            + activity.getDurationInMillis() + " ms");
                    }
                }
                if (endActivity != null) {
                    System.out.println("-- " + endActivity.getActivityName()
                        + " [" + endActivity.getActivityId() + "] "
                        + endActivity.getDurationInMillis() + " ms");
                    System.out.println("流程处理完成 " + processDefinition.getName() + " ["
                        + processInstance.getProcessDefinitionKey() + "] "
                        + endActivity.getEndTime());
                }
            }
            // Re-query the process instance, making sure the latest state is available
            processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processInstance.getId()).singleResult();
        }
        scanner.close();
    }
}

```

运行查看日志,可以发现默认使用h2的sql脚本创建了一系列的表结构：

```bash
17:13:29.420 [main] DEBUG o.a.e.i.c.ProcessEngineConfigurationImpl - initializing datasource to db: jdbc:h2:mem:activiti;DB_CLOSE_DELAY=1000
17:13:29.430 [main] DEBUG org.apache.ibatis.logging.LogFactory - Logging initialized using 'class org.apache.ibatis.logging.slf4j.Slf4jImpl' adapter.
17:13:29.446 [main] DEBUG o.a.i.d.pooled.PooledDataSource - PooledDataSource forcefully closed/removed all connections.
17:13:29.628 [main] DEBUG o.a.i.d.pooled.PooledDataSource - Created connection 1351478315.
17:13:29.632 [main] DEBUG o.a.e.i.c.ProcessEngineConfigurationImpl - database product name: 'H2'
17:13:29.632 [main] DEBUG o.a.e.i.c.ProcessEngineConfigurationImpl - using database type: h2
17:13:29.633 [main] DEBUG o.a.i.d.pooled.PooledDataSource - Returned connection 1351478315 to pool.
17:13:30.506 [main] DEBUG o.a.e.i.interceptor.LogInterceptor - 

17:13:30.506 [main] DEBUG o.a.e.i.interceptor.LogInterceptor - --- starting SchemaOperationsProcessEngineBuild --------------------------------------------------------
17:13:30.514 [main] DEBUG o.a.i.t.jdbc.JdbcTransaction - Opening JDBC Connection
17:13:30.515 [main] DEBUG o.a.i.d.pooled.PooledDataSource - Checked out connection 1351478315 from pool.
17:13:30.515 [main] DEBUG o.a.i.t.jdbc.JdbcTransaction - Setting autocommit to false on JDBC Connection [conn0: url=jdbc:h2:mem:activiti user=SA]
17:13:30.540 [main] INFO  o.a.engine.impl.db.DbSqlSession - performing create on engine with resource org/activiti/db/create/activiti.h2.create.engine.sql
17:13:30.540 [main] DEBUG o.a.engine.impl.db.DbSqlSession - SQL: create table ACT_GE_PROPERTY ( 
NAME_ varchar(64), 
VALUE_ varchar(300), 
REV_ integer, 
primary key (NAME_) 
)
17:13:30.546 [main] DEBUG o.a.engine.impl.db.DbSqlSession - SQL: insert into ACT_GE_PROPERTY 
values ('schema.version', '5.22.0.0', 1)
17:13:30.547 [main] DEBUG o.a.engine.impl.db.DbSqlSession - SQL: insert into ACT_GE_PROPERTY 
values ('schema.history', 'create(5.22.0.0)', 1)
17:13:30.548 [main] DEBUG o.a.engine.impl.db.DbSqlSession - SQL: insert into ACT_GE_PROPERTY 
values ('next.dbid', '1', 1)

# 中间省略创建表的过程
....

17:13:30.638 [main] INFO  o.a.engine.impl.ProcessEngineImpl - ProcessEngine default created
17:13:30.643 [main] INFO  samples.activiti.OnboardingRequest - ProcessEngine Name: [ default ] Version: [ 5.22.0.0 ]
Disconnected from the target VM, address: '127.0.0.1:53308', transport: 'socket'

Process finished with exit code 0

```
## 未成年人处理

```java
package samples.activiti;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

import java.util.Date;

/**
 * 未成年处理流程, 如果未成年, 劝其离开
 *
 * @author zilaiye
 * @date 2017/10/27
 */
public class YoungManProcess implements JavaDelegate {
    public void execute(DelegateExecution delegateExecution) throws Exception {
        Date now = new Date();
        delegateExecution.setVariable("autoWelcomeTime", now);
        System.out.println("未成年处理流程: 对不起:["
            + delegateExecution.getVariable("fullName") + "], 你还未成年,再长大点再来吧。");
    }

}
```

## 流程文件：logon-bpmn20.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:activiti="http://activiti.org/bpmn" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC" xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" typeLanguage="http://www.w3.org/2001/XMLSchema" expressionLanguage="http://www.w3.org/1999/XPath" targetNamespace="http://www.activiti.org/processdef">
    <process id="yellow-site-logon" name="YellowSiteLogon" isExecutable="true">
        <!-- 开始事件 -->
        <startEvent id="startLogon" name="Start" activiti:initiator="initiator"></startEvent>
        <!-- 输入用户信息任务 -->
        <userTask id="inputUserInfo" name="请输入用户信息:" activiti:assignee="${initiator}" activiti:candidateGroups="managers">
            <extensionElements>
                <activiti:formProperty id="fullName" name="请输入姓名" type="string"></activiti:formProperty>
                <activiti:formProperty id="age" name="请输入年龄" type="long" required="true"></activiti:formProperty>
            </extensionElements>
        </userTask>
        <!-- 连接开始事件和输入信息的流程线 -->
        <sequenceFlow id="sid-1337EA98-7364-4198-B5D9-30F5341D6918" sourceRef="startLogon" targetRef="inputUserInfo"></sequenceFlow>

        <!-- 唯一的判断网关, 默认未满18岁处理 -->
        <exclusiveGateway id="decision" name="判断年龄是否满18岁" default="youngManPath"></exclusiveGateway>
        <!-- 输入用户信息, 判断 -->
        <sequenceFlow id="sid-42BE5661-C3D5-4DE6-96F5-73D34822727A" sourceRef="inputUserInfo" targetRef="decision"></sequenceFlow>

        <!--<scriptTask id="automatedIntro" name="Generic and Automated Data Entry" scriptFormat="javascript" activiti:autoStoreVariables="false">-->
            <!--<script><![CDATA[var dateAsString = new Date().toString();-->
<!--execution.setVariable("autoWelcomeTime", dateAsString);]]></script>-->
        <!--</scriptTask>-->
        <!-- 未成年分支 -->
        <sequenceFlow id="youngManPath" sourceRef="decision" targetRef="youngManIntro"></sequenceFlow>
        <serviceTask id="youngManIntro" name="对不起,你未成年,不能登录此网站" activiti:class="samples.activiti.YoungManProcess"></serviceTask>
        <sequenceFlow id="sid-37A73ACA-2E23-400B-96F3-71F77738DAFA" sourceRef="youngManIntro" targetRef="endLogon"></sequenceFlow>


        <!-- 如果已成年,欢迎之 -->
        <sequenceFlow id="manPath" name="&gt;18" sourceRef="decision" targetRef="manIntro">
            <conditionExpression xsi:type="tFormalExpression"><![CDATA[${age >= 18}]]></conditionExpression>
        </sequenceFlow>
        <userTask id="manIntro" name="用户已成年,欢迎其进入成人世界" activiti:assignee="${initiator}" activiti:candidateGroups="managers">
            <extensionElements>
                <activiti:formProperty id="welcomeManTime" name="欢迎您,你已通过验证,输入你的生日,开启一段愉快旅程" type="date" datePattern="MM-dd-yyyy hh:mm"></activiti:formProperty>
            </extensionElements>
        </userTask>
        <sequenceFlow id="sid-BA6F061B-47B6-428B-8CE6-739244B14BD6" sourceRef="manIntro" targetRef="endLogon"></sequenceFlow>
        <!-- 结束事件 -->
        <endEvent id="endLogon" name="End"></endEvent>
    </process>
    <bpmndi:BPMNDiagram id="BPMNDiagram_onboarding">
        <bpmndi:BPMNPlane bpmnElement="yellow-site-logon" id="BPMNPlane_onboarding">
            <bpmndi:BPMNShape bpmnElement="startLogon" id="BPMNShape_startOnboarding">
                <omgdc:Bounds height="30.0" width="30.0" x="155.0" y="145.0"></omgdc:Bounds>
            </bpmndi:BPMNShape>
            <bpmndi:BPMNShape bpmnElement="inputUserInfo" id="BPMNShape_enterOnboardingData">
                <omgdc:Bounds height="80.0" width="100.0" x="240.0" y="120.0"></omgdc:Bounds>
            </bpmndi:BPMNShape>
            <bpmndi:BPMNShape bpmnElement="decision" id="BPMNShape_decision">
                <omgdc:Bounds height="40.0" width="40.0" x="385.0" y="140.0"></omgdc:Bounds>
            </bpmndi:BPMNShape>
            <bpmndi:BPMNShape bpmnElement="manIntro" id="BPMNShape_personalizedIntro">
                <omgdc:Bounds height="80.0" width="100.0" x="519.0" y="15.0"></omgdc:Bounds>
            </bpmndi:BPMNShape>
            <bpmndi:BPMNShape bpmnElement="endLogon" id="BPMNShape_endOnboarding">
                <omgdc:Bounds height="28.0" width="28.0" x="725.0" y="165.0"></omgdc:Bounds>
            </bpmndi:BPMNShape>
            <bpmndi:BPMNShape bpmnElement="youngManIntro" id="BPMNShape_automatedIntro">
                <omgdc:Bounds height="80.0" width="100.0" x="520.0" y="255.0"></omgdc:Bounds>
            </bpmndi:BPMNShape>
            <bpmndi:BPMNEdge bpmnElement="sid-37A73ACA-2E23-400B-96F3-71F77738DAFA" id="BPMNEdge_sid-37A73ACA-2E23-400B-96F3-71F77738DAFA">
                <omgdi:waypoint x="570.0" y="255.0"></omgdi:waypoint>
                <omgdi:waypoint x="570.0" y="179.0"></omgdi:waypoint>
                <omgdi:waypoint x="725.0" y="179.0"></omgdi:waypoint>
            </bpmndi:BPMNEdge>
            <bpmndi:BPMNEdge bpmnElement="sid-1337EA98-7364-4198-B5D9-30F5341D6918" id="BPMNEdge_sid-1337EA98-7364-4198-B5D9-30F5341D6918">
                <omgdi:waypoint x="185.0" y="160.0"></omgdi:waypoint>
                <omgdi:waypoint x="240.0" y="160.0"></omgdi:waypoint>
            </bpmndi:BPMNEdge>
            <bpmndi:BPMNEdge bpmnElement="automatedIntroPath" id="BPMNEdge_automatedIntroPath">
                <omgdi:waypoint x="405.0" y="180.0"></omgdi:waypoint>
                <omgdi:waypoint x="405.0" y="295.0"></omgdi:waypoint>
                <omgdi:waypoint x="520.0" y="295.0"></omgdi:waypoint>
            </bpmndi:BPMNEdge>
            <bpmndi:BPMNEdge bpmnElement="personalizedIntroPath" id="BPMNEdge_personalizedIntroPath">
                <omgdi:waypoint x="405.0" y="140.0"></omgdi:waypoint>
                <omgdi:waypoint x="405.0" y="55.0"></omgdi:waypoint>
                <omgdi:waypoint x="519.0" y="55.0"></omgdi:waypoint>
            </bpmndi:BPMNEdge>
            <bpmndi:BPMNEdge bpmnElement="sid-42BE5661-C3D5-4DE6-96F5-73D34822727A" id="BPMNEdge_sid-42BE5661-C3D5-4DE6-96F5-73D34822727A">
                <omgdi:waypoint x="340.0" y="160.0"></omgdi:waypoint>
                <omgdi:waypoint x="385.0" y="160.0"></omgdi:waypoint>
            </bpmndi:BPMNEdge>
            <bpmndi:BPMNEdge bpmnElement="sid-BA6F061B-47B6-428B-8CE6-739244B14BD6" id="BPMNEdge_sid-BA6F061B-47B6-428B-8CE6-739244B14BD6">
                <omgdi:waypoint x="619.0" y="55.0"></omgdi:waypoint>
                <omgdi:waypoint x="739.0" y="55.0"></omgdi:waypoint>
                <omgdi:waypoint x="739.0" y="165.0"></omgdi:waypoint>
            </bpmndi:BPMNEdge>
        </bpmndi:BPMNPlane>
    </bpmndi:BPMNDiagram>
</definitions>
```

## 运行结果

### 未成年处理

```bash
19:02:45.299 [main] INFO  o.a.engine.impl.db.DbSqlSession - performing create on engine with resource org/activiti/db/create/activiti.h2.create.engine.sql
19:02:45.361 [main] INFO  o.a.engine.impl.db.DbSqlSession - performing create on history with resource org/activiti/db/create/activiti.h2.create.history.sql
19:02:45.376 [main] INFO  o.a.engine.impl.db.DbSqlSession - performing create on identity with resource org/activiti/db/create/activiti.h2.create.identity.sql
19:02:45.382 [main] INFO  o.a.engine.impl.ProcessEngineImpl - ProcessEngine default created
获取流程引擎对象完成: ProcessEngine Name: [ default ] Version: [ 5.22.0.0 ]
19:02:45.409 [main] INFO  o.a.e.i.bpmn.deployer.BpmnDeployer - Processing resource logon-bpmn20.xml
发现流程定义, 名称:[YellowSiteLogon] Id:[yellow-site-logon:1:4]
登录判断流程开始,流程实例ID:[yellow-site-logon:1:4] key:[yellow-site-logon]
此流程共有任务数量: [ 1]
开始执行流程任务: [请输入用户信息:]
请输入姓名?
zilaiye
请输入年龄? (Must be a whole number)
12
未成年处理流程: 对不起:[zilaiye], 你还未成年,再长大点再来吧。
流程处理完成, 现在开始回放处理过程:
流程处理开始, 实例名称:[YellowSiteLogon] Key:[yellow-site-logon] 开始处理时间: [Sun Oct 29 19:02:47 CST 2017]
-- Start [startLogon] 7 ms
-- 请输入用户信息: [inputUserInfo] 17307 ms
-- 判断年龄是否满18岁 [decision] 8 ms
-- 对不起,你未成年,不能登录此网站 [youngManIntro] 1 ms
-- End [endLogon] 0 ms
流程处理完成 YellowSiteLogon [yellow-site-logon] Sun Oct 29 19:03:04 CST 2017

```

### 已成年处理

```bash
19:04:00.745 [main] INFO  o.a.engine.impl.db.DbSqlSession - performing create on engine with resource org/activiti/db/create/activiti.h2.create.engine.sql
19:04:00.808 [main] INFO  o.a.engine.impl.db.DbSqlSession - performing create on history with resource org/activiti/db/create/activiti.h2.create.history.sql
19:04:00.822 [main] INFO  o.a.engine.impl.db.DbSqlSession - performing create on identity with resource org/activiti/db/create/activiti.h2.create.identity.sql
19:04:00.827 [main] INFO  o.a.engine.impl.ProcessEngineImpl - ProcessEngine default created
获取流程引擎对象完成: ProcessEngine Name: [ default ] Version: [ 5.22.0.0 ]
19:04:00.853 [main] INFO  o.a.e.i.bpmn.deployer.BpmnDeployer - Processing resource logon-bpmn20.xml
发现流程定义, 名称:[YellowSiteLogon] Id:[yellow-site-logon:1:4]
登录判断流程开始,流程实例ID:[yellow-site-logon:1:4] key:[yellow-site-logon]
此流程共有任务数量: [ 1]
开始执行流程任务: [请输入用户信息:]
请输入姓名?
zilaiye
请输入年龄? (Must be a whole number)
20
流程处理完成, 现在开始回放处理过程:
流程处理开始, 实例名称:[YellowSiteLogon] Key:[yellow-site-logon] 开始处理时间: [Sun Oct 29 19:04:02 CST 2017]
-- Start [startLogon] 6 ms
-- 请输入用户信息: [inputUserInfo] 5685 ms
-- 判断年龄是否满18岁 [decision] 8 ms
此流程共有任务数量: [ 1]
开始执行流程任务: [用户已成年,欢迎其进入成人世界]
欢迎您,你已通过验证,输入你的生日,开启一段愉快旅程? (Must be a date m/d/yy)
1/1/94
流程处理完成, 现在开始回放处理过程:
流程处理开始, 实例名称:[YellowSiteLogon] Key:[yellow-site-logon] 开始处理时间: [Sun Oct 29 19:04:02 CST 2017]
-- Start [startLogon] 6 ms
-- 请输入用户信息: [inputUserInfo] 5685 ms
-- 判断年龄是否满18岁 [decision] 8 ms
-- 用户已成年,欢迎其进入成人世界 [manIntro] 7513 ms
-- End [endLogon] 1 ms
流程处理完成 YellowSiteLogon [yellow-site-logon] Sun Oct 29 19:04:16 CST 2017
```

# 参考资料

* [Activiti Quick Start](https://www.activiti.org/quick-start)