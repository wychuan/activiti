# Creating a ProcessEngine创建 ProcessEngine

## 使用配置文件创建ProcessEngine

### 配置文件
在classpath目录下创建`activiti.cfg.xml`配置文件，内容如下：
```xml
<beans xmlns="http://www.springframework.org/schema/beans"
	   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	   xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

	<bean id="processEngineConfiguration" class="org.activiti.engine.impl.cfg.StandaloneProcessEngineConfiguration">

		<property name="jdbcUrl" value="jdbc:h2:mem:activiti;DB_CLOSE_DELAY=1000" />
		<property name="jdbcDriver" value="org.h2.Driver" />
		<property name="jdbcUsername" value="sa" />
		<property name="jdbcPassword" value="" />

		<property name="databaseSchemaUpdate" value="true" />

		<property name="asyncExecutorActivate" value="false" />

		<!--<property name="mailServerHost" value="mail.my-corp.com" />-->
		<!--<property name="mailServerPort" value="5025" />-->
	</bean>

</beans>

> 注意：activiti.cfg.xml 必须包含一个 bean, id为'processEngineConfiguration'。
```

### 使用`org.activiti.engine.ProcessEngines`类来获取一个ProcessEngine流程引擎：

```java
/**
 * 1. 需要在classpath目录下创建activiti.cfg.xml配置文件
 * 2. 此方法默认会在classpath下搜索activiti.cfg.xml配置文件,并使用其配置构建引擎。
 * 3. 如果在classpath目录下没有找到activiti.cfg.xml配置文件,则引擎对象为null
 */
@Test
public void getDefaultProcessEngine() {
    ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
    assertNotNull(processEngine);
    assertEquals("default", processEngine.getName());
}
```

此方法会在 classpath 下搜索 activiti.cfg.xml， 并基于这个文件中的配置构建引擎。

### 使用`org.activiti.engine.ProcessEngineConfiguration`来创建

ProcessEngineConfiguration对象提供了一些方法，用于编程方式创建ProcessEngine引擎，默认bean id为`processEngineConfiguration`，此类提供了使用不同的bean id的方法：

```java
public static ProcessEngineConfiguration createProcessEngineConfigurationFromResourceDefault() {
	return createProcessEngineConfigurationFromResource("activiti.cfg.xml", "processEngineConfiguration");
}

public static ProcessEngineConfiguration createProcessEngineConfigurationFromResource(String resource) {
	return createProcessEngineConfigurationFromResource(resource, "processEngineConfiguration");
}

public static ProcessEngineConfiguration createProcessEngineConfigurationFromResource(String resource, String beanName) {
	return BeansConfigurationHelper.parseProcessEngineConfigurationFromResource(resource, beanName);
}

public static ProcessEngineConfiguration createProcessEngineConfigurationFromInputStream(InputStream inputStream) {
	return createProcessEngineConfigurationFromInputStream(inputStream, "processEngineConfiguration");
}

public static ProcessEngineConfiguration createProcessEngineConfigurationFromInputStream(InputStream inputStream, String beanName) {
	return BeansConfigurationHelper.parseProcessEngineConfigurationFromInputStream(inputStream, beanName);
}
```

如下所示：

```java
/**
 * 1. ProcessEngineConfiguration提供了一系列的.createXXX() 方法用于创建流程引擎,其返回配置对象本身。
 * 2. 可以通过更改参数更改配置
 * 3. 通过buildProcessEngine方法来获取一个processEngine对象
 */
@Test
public void getProcessEngineByConfigObject(){
//        ProcessEngine processEngine = ProcessEngineConfiguration.createProcessEngineConfigurationFromResourceDefault()
//            .buildProcessEngine();

    ProcessEngine processEngine = ProcessEngineConfiguration.createProcessEngineConfigurationFromResourceDefault()
        .setJdbcDriver("com.mysql.jdbc.Driver")
        .setJdbcUsername("dbadmin")
        .setJdbcPassword("2016admin*JYW")
        .setJdbcUrl("jdbc:mysql://mysql.master.dev.7mxing.com:3306/activiti?useSSL=false")
        .setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE)
        .buildProcessEngine();

    assertNotNull(processEngine);
    assertEquals("default", processEngine.getName());
}
```

## 非配置文件创建

### 使用ProcessEngineConfiguration对象提供的默认方法

```java
public static ProcessEngineConfiguration createStandaloneProcessEngineConfiguration() {
    return new StandaloneProcessEngineConfiguration();
}

public static ProcessEngineConfiguration createStandaloneInMemProcessEngineConfiguration() {
    return new StandaloneInMemProcessEngineConfiguration();
}
```

### new 一个StandaloneProcessEngineConfiguration

```java
/**
 * 1. 手动创建一个StandaloneProcessEngineConfiguration对象
 * 2. 调用其buildProcessEngine()方法获取ProcessEngine对象
 */
@Test
public void getProcessEngineCode(){
    ProcessEngine processEngine = new StandaloneProcessEngineConfiguration()
        .setJdbcDriver("com.mysql.jdbc.Driver")
        .setJdbcUsername("dbadmin")
        .setJdbcPassword("2016admin*JYW")
        .setJdbcUrl("jdbc:mysql://mysql.master.dev.7mxing.com:3306/activiti?useSSL=false")
        .setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE)
        .buildProcessEngine();
    assertNotNull(processEngine);
    assertEquals("default", processEngine.getName());
}
```

## Spring集成

### Spring配置

### SpringBoot

## ProcessEngineConfiguration bean

activiti.cfg.xml 必须包含一个 bean, id为'processEngineConfiguration'。

```xml
<bean id="processEngineConfiguration" class="org.activiti.engine.impl.cfg.StandaloneProcessEngineConfiguration">
```

这个 bean 会用来构建 ProcessEngine。 有多个类可以用来定义processEngineConfiguration。 这些类对应不同的环境，并设置了对应的默认值。 最好选择（最）适用于你的环境的类， 这样可以少配置几个引擎的参数。 下面是目前可以使用的类（以后会包含更多）：

![ProcessEngineConfigurationImpl](/imgs/ProcessEngineConfiguration-uml.jpeg)

* org.activiti.engine.impl.cfg.StandaloneProcessEngineConfiguration: 单独运行的 流程引擎。Activiti 会自己处理事务。 默认，数据库只在引擎启动时检测 （如果没有 Activiti 的表或者表结构不正确就会抛出异常）。
* org.activiti.engine.impl.cfg.StandaloneInMemProcessEngineConfiguration: 单元测试时的辅助类。Activiti 会自己控制事务。 默认使用 H2 内存数据库。数据库表会在引擎启动时创建，关闭时删除。 使用它时，不需要其他配置（除非使用 job 执行器或邮件功 能）。
* org.activiti.spring.SpringProcessEngineConfiguration: 在Spring 环境下使用流程引擎。 参考 Chapter 5. Spring integration 集成 Spring。
* org.activiti.engine.impl.cfg.JtaProcessEngineConfiguration: 单独运行流程引擎，并使用 JTA 事务。

# 参考

* https://www.activiti.org/userguide/index.html#configuration
