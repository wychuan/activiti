# Activiti API and Services

引擎API是我们与Activiti交互的最常用方式。在activiti中，`ProcessEngine`作为中心起始点，可以从中获取很多囊括工作流/BPM 方法的服务(Services)。`ProcessEngine`和服务类都是线程安全的，所以建议在整个项目中仅保持它们的一个引用即可：

![api and service](/imgs/api.services.png)

```java
ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

RuntimeService runtimeService = processEngine.getRuntimeService();
RepositoryService repositoryService = processEngine.getRepositoryService();
TaskService taskService = processEngine.getTaskService();
ManagementService managementService = processEngine.getManagementService();
IdentityService identityService = processEngine.getIdentityService();
HistoryService historyService = processEngine.getHistoryService();
FormService formService = processEngine.getFormService();
DynamicBpmnService dynamicBpmnService = processEngine.getDynamicBpmnService();
```

上图中的服务类是Activiti API的核心，各个类的概要说明如下：

## 概念与对象

先了解一下Activiti里的常用概念：

### Deployment发布包

> Original: A deployment is a container for resources such as process definitions, images, forms, etc.

发布包是 Activiti 引擎的打包单位，一个发布包可以包含多个流程定义 BPMN 2.0 xml 文件，图片文件和其他资源。

Deployment 在 Activiti 中的源码定义如下：

```java
public interface Deployment {

  String getId();
  
  String getName();
  
  Date getDeploymentTime();
  
  String getCategory();
  
  String getTenantId();
  
}
```

** Deployment ** 对应的表结构定义如下：

```sql
CREATE TABLE `ACT_RE_DEPLOYMENT` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  `DEPLOY_TIME_` timestamp(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```

### ProcessDefinition流程定义

> Original: An object structure representing an executable process composed of activities and transitions.
> Business processes are often created with graphical editors that store the process definition in certain file format. These files can be added to a {@link Deployment} artifact, such as for example a Business Archive (.bar) file.
> At deploy time, the engine will then parse the process definition files to an executable instance of this class, that can be used to start a {@link ProcessInstance}.

Java定义：

```java
public interface ProcessDefinition {

  /** unique identifier */
  String getId();

  /** category name which is derived from the targetNamespace attribute in the definitions element */
  String getCategory();
  
  /** label used for display purposes */
  String getName();
  
  /** unique name for all versions this process definitions */
  String getKey();
  
  /** description of this process **/
  String getDescription();
  
  /** version of this process definition */
  int getVersion();

  /** name of {@link RepositoryService#getResourceAsStream(String, String) the resource} 
   * of this process definition. */
  String getResourceName();

  /** The deployment in which this process definition is contained. */
  String getDeploymentId();
  
  /** The resource name in the deployment of the diagram image (if any). */
  String getDiagramResourceName();

  /** Does this process definition has a {@link FormService#getStartFormData(String) start form key}. */
  boolean hasStartFormKey();
  
  /** Does this process definition has a graphical notation defined (such that a diagram can be generated)? */
  boolean hasGraphicalNotation();
  
  /** Returns true if the process definition is in suspended state. */
  boolean isSuspended();
  
  /** The tenant identifier of this process definition */
  String getTenantId();
  
}
```

表定义：

```sql
CREATE TABLE `ACT_RE_PROCDEF` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `KEY_` varchar(255) COLLATE utf8_bin NOT NULL,
  `VERSION_` int(11) NOT NULL,
  `DEPLOYMENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `RESOURCE_NAME_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `DGRM_RESOURCE_NAME_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `HAS_START_FORM_KEY_` tinyint(4) DEFAULT NULL,
  `HAS_GRAPHICAL_NOTATION_` tinyint(4) DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `ACT_UNIQ_PROCDEF` (`KEY_`,`VERSION_`,`TENANT_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
```


## ProcessEngine


## RepositoryService

> Original: Service providing access to the repository of process definitions and deployments.

** RepositoryService ** 用来管理流程的静态资源信息，包括流程的定义信息及其发布信息。流程定义是 BPMN 2.0的Java实现，它包含了一个流程每个环节的结构和行为。

一般来说，我们使用 BPMN 2.0 xml文件来定义一个流程，定义完成后可以将其打包并发布到流程引擎中。可以通过 DeploymentBuilder 来创建和发布：

```java
@Test
public void deployDeployment(){
    ProcessEngine processEngine = ProcessEngineConfiguration.createProcessEngineConfigurationFromResourceDefault()
        .buildProcessEngine();

    RepositoryService repositoryService = processEngine.getRepositoryService();

    Deployment deploy = repositoryService.createDeployment()
        .addClasspathResource("logon-bpmn20.xml")
        .name("登录用户验证流程")
        .deploy();
    logger.info("deploy: id={} name={} category={} tenantId={} deploymentTime={}", deploy.getId(), deploy.getName(), deploy.getCategory(), deploy.getTenantId(), deploy.getDeploymentTime());
}
```

查看日志可以发现，此操作主要向三张表中插入了数据：

```bash
11:03:29.230 [main] DEBUG o.a.engine.impl.db.DbSqlSession - inserting: ProcessDefinitionEntity[yellow-site-logon:2:17504]
# 向 ACT_RE_PROCDEF 流程定义表插入一条流程数据
11:03:29.230 [main] DEBUG o.a.e.i.p.e.P.insertProcessDefinition - ==>  Preparing: insert into ACT_RE_PROCDEF(ID_, REV_, CATEGORY_, NAME_, KEY_, VERSION_, DEPLOYMENT_ID_, RESOURCE_NAME_, DGRM_RESOURCE_NAME_, DESCRIPTION_, HAS_START_FORM_KEY_, HAS_GRAPHICAL_NOTATION_ , SUSPENSION_STATE_, TENANT_ID_) values (?, 1, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) 
11:03:29.232 [main] DEBUG o.a.e.i.p.e.P.insertProcessDefinition - ==> Parameters: yellow-site-logon:2:17504(String), http://www.activiti.org/processdef(String), YellowSiteLogon(String), yellow-site-logon(String), 2(Integer), 17501(String), logon-bpmn20.xml(String), logon-yellow-site-logon.png(String), null, false(Boolean), true(Boolean), 1(Integer), (String)
11:03:29.248 [main] DEBUG o.a.e.i.p.e.P.insertProcessDefinition - <==    Updates: 1
11:03:29.248 [main] DEBUG o.a.engine.impl.db.DbSqlSession - inserting: DeploymentEntity[id=17501, name=登录用户验证流程]

# 向ACT_RE_DEPLOYMENT 发布包表插入一条流程发布信息
11:03:29.248 [main] DEBUG o.a.e.i.p.e.D.insertDeployment - ==>  Preparing: insert into ACT_RE_DEPLOYMENT(ID_, NAME_, CATEGORY_, TENANT_ID_, DEPLOY_TIME_) values(?, ?, ?, ?, ?) 
11:03:29.250 [main] DEBUG o.a.e.i.p.e.D.insertDeployment - ==> Parameters: 17501(String), 登录用户验证流程(String), null, (String), 2017-10-31 11:03:27.25(Timestamp)
11:03:29.265 [main] DEBUG o.a.e.i.p.e.D.insertDeployment - <==    Updates: 1

# 把 BPMN 2.0 xml定义文件插入数据库，把生成的 流程图片插入数据库表 ACT_GE_BYTEARRAY
11:03:29.268 [main] DEBUG o.a.e.i.p.e.R.bulkInsertResource - ==>  Preparing: INSERT INTO ACT_GE_BYTEARRAY(ID_, REV_, NAME_, BYTES_, DEPLOYMENT_ID_, GENERATED_) VALUES (?, 1, ?, ?, ?, ?) , (?, 1, ?, ?, ?, ?) 
11:03:29.268 [main] DEBUG o.a.e.i.p.e.R.bulkInsertResource - ==> Parameters: 17502(String), logon-bpmn20.xml(String), java.io.ByteArrayInputStream@4867ab9f(ByteArrayInputStream), 17501(String), false(Boolean), 17503(String), logon-yellow-site-logon.png(String), java.io.ByteArrayInputStream@65f2f9b0(ByteArrayInputStream), 17501(String), true(Boolean)
11:03:29.307 [main] DEBUG o.a.e.i.p.e.R.bulkInsertResource - <==    Updates: 2
```


发布一个发布包，意味着把其上传到引擎中，所有xml定义的流程信息都会在保存进数据库之前分析解析成结构化数据。发布完成之后，系统便知道了此流程的存在，此流程就已经可以启动了。

除此之外，RepositoryService可以：

### 查询引擎中的发布包和流程定义

	```java
	@Test
	public void queryDeployAndProcDef(){
	    ProcessEngine processEngine = ProcessEngineConfiguration.createProcessEngineConfigurationFromResourceDefault()
	        .buildProcessEngine();

	    RepositoryService repositoryService = processEngine.getRepositoryService();

	    List<Deployment> lstDeployments = repositoryService.createDeploymentQuery().list();
	    logger.info("deployments count:{}", lstDeployments.size());
	    for (Deployment deployment : lstDeployments) {
	        logger.info("deploy: id={} name={} category={} tenantId={} deploymentTime={}", deployment.getId(), deployment.getName(), deployment.getCategory(), deployment.getTenantId(), deployment.getDeploymentTime());
	        List<ProcessDefinition> lstProcessDef = repositoryService.createProcessDefinitionQuery().deploymentId(deployment.getId()).list();
	        for (ProcessDefinition processDefinition : lstProcessDef) {
	            logger.info("deploy: name={} process name:{}", deployment.getName(), processDefinition.getName());
	        }
	        break;
	    }
	}
	```

* 暂停或激活发布包，对应全部和特定流程定义。 暂停意味着它们不能再执行任何操作了，激活是对应的反向操作。
* 获得多种资源，像是包含在发布包里的文件，或引擎自动生成的流程图。
* 获得流程定义的pojo版本， 可以用来通过java解析流程，而不必通过xml。

## RuntimeService

正如 RepositoryService 负责静态信息（比如，不会改变的数据，至少是不怎么改变的），**RuntimeService **正好是完全相反的。它负责启动一个流程定义的新实例。流程定义定义了流程各个节点的结构和行为。 流程实例就是这样一个流程定义的实例。可以把 RepositoryService理解为负责类的定义和编译，而RuntimeService负责在运行时创建类的实例。

RuntimeService 也可以用来获取和保存流程变量。 这些数据是特定于某个流程实例的，并会被很多流程中的节点使用 （比如，一个排他网关常常使用流程变量来决定选择哪条路径继续流程）。 Runtimeservice 也能查询流程实例和执行。 执行对应 BPMN 2.0 中的'token'。基本上执行指向流程实例当前在哪里。 最后，RuntimeService 可以在流程实例等待外部触发时使用，这时可以用来继续流程实例。 流程实例可以有很多暂停状态，而服务提供了多种方法来'触发'实例， 接受外部触发后，流程实例就会继续向下执行。
